# Le module "discord" est un wrapper de l'API Discord.
# C'est à dire qu'il contient un ensemble de méthodes implémentées en python qui permettent de communiquer avec l'API Discord.
import discord
from chrome import setup_chrome
from discord.ext import commands

# Le module "requests" permet d'effectuer des requêtes HTTP (GET, POST, ...)
import requests
import json
import random

from constantes import DISCORD_BOT_TOKEN
from constantes import JOKE_CATEGORIES

# Création d'un client à partir de la classe discord.Client issue du module "discord.py"
# Le préfix pour appeler une commande est ici le caractère "!"
bot = commands.Bot(command_prefix='!', intents=discord.Intents.all())

# Cet événement est déclenché par Discord une fois que l'initialisation du bot et sa connexion se sont déroulés avec succès.
# Note 1 : Le décorateur @bot.event permet d'indiquer que la fonction on_ready est une fonction qui doit recevoir les informations envoyées lorsque l'événement est appelé par Discord.
# Note 2 : Le mot clé async définie la fonction comme une coroutine. Le concept de coroutine est un concept assez avancé que vous n'avez pas besoin de maîtriser sur le bout des doigts pour créer un bot. Sachez seulement que devant les fonctions qui gèrent ces événements, vous devez utiliser le mot clé async.
@bot.event
async def on_ready():
    print("[+] Le bot est connecté au salon.")
    setup_chrome()


# À chaque fois qu'un message est posté sur le serveur, Discord va déclencher l'événement on_message.
# Pour réagir à un message posté sur le serveur, il suffit donc d'implémenter une fonction pour gérer cet événement (comme on vient de le faire avec on_ready)
@bot.event
async def on_message(message):
    print("[+] Nouveau message reçu dans le chat : " + message.content)

    # La fonction on_message est la première fonction appelée lors de la réception d'un message sur le serveur.
    # Il faut donc ajouter la ligne suivante en dernière ligne de la méthode pour permettre aux fonctions gérant des commandes d'être évaluées ensuite.
    await bot.process_commands(message)


# On peut définir des commandes avec le décorateur @bot.command(name="nom_de_la_commande")
# Cette fonction est déclenchée avec la commande !ping
# Le bot renvoie "pong" sur le chat.
@bot.command(name="ping")
async def heartbeat_check(ctx):
    print("[+] Commande 'ping' reçue")
    # On renvoie "pong" dans le chat
    await ctx.send("pong")

# Commande pour lister les catégories

@bot.command(name="cat")
async def heartbeat_check(ctx):
    print("[+] Commande 'cat' reçue")
    i = 0
    await ctx.send("Liste des catégories : ")
    for i in range(15):
        await ctx.send("Catégorie : " + JOKE_CATEGORIES[i])

# Commande pour ouvrir Google chrome

@bot.command(name="chrome")
async def heartbeat_check(ctx):
    """
    Le bot discord nous ouvre un navigateur Chrome
    """
    print("[+] Commande 'chrome' reçue")
    setup_chrome()
    await ctx.send("Ouverture du navigateur Chrome")
        
# Commande pour lister les commandes du bot

@bot.command(name="commands")
async def heartbeat_check(ctx):
    print("[+] Commande 'commands' reçue")
    await ctx.send("Liste des commandes : ")
    await ctx.send("- !commands - Liste les commandes \n- !cat - Liste des catégories \n- !joke - Génère une blague")
    
# Commande pour générer une blague
@bot.command(name="joke")
async def make_a_joke(ctx, *args):
    # La variable "args" est un tuple. Elle contient l'ensemble des arguments passés en paramètre de la commande.
    # On peut les récupérer comme pour un liste avec args[0], args[1], args[2], ...
    if not args:
        rdm = random.choice(JOKE_CATEGORIES)
        response = requests.get('https://api.chucknorris.io/jokes/random?category=' + rdm)
        joke = json.loads(response.text)
        await ctx.channel.send("Catégorie : " + rdm + " Blague : " + joke['value'])
    else:
        response = requests.get('https://api.chucknorris.io/jokes/random?category=' + args[0])
        joke = json.loads(response.text)
        await ctx.channel.send(joke['value'])


# Connexion du bot au salon discord.
# Le jeton passé en paramètre référence un application Discord, elle-même associée à un serveur.
client = discord.Client(intents=discord.Intents.default())
bot.run(DISCORD_BOT_TOKEN)


