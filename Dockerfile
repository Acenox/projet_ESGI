FROM python:latest

WORKDIR /app
COPY . /app
RUN apt update && \
    apt upgrade -y
RUN pip install -r requirements.txt

CMD ["python", "bot.py"]
