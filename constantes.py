# Liste des catégories disponibles pour les thématiques de blagues.
JOKE_CATEGORIES = [
"animal",
"career",
"celebrity",
"dev",
"explicit",
"fashion",
"food",
"history",
"money",
"movie",
"music",
"political",
"religion",
"science",
"sport",
"travel"
]


# Vous pouvez récupérer une blague aléatoirement depuis cette url : https://api.chucknorris.io/jokes/random?category={category}
# Cela veut dire que vous pouvez l'appeler via HTTP/GET et passer un paramètre "category" pour choisir la bonne catégorie.
JOKE_URL_BASE = "https://api.chucknorris.io/jokes/random"


# Vous devez remplacer cette variable par le "token" de votre propre bot.
# Pour le récupérer, il faut aller à cette url "https://discord.com/developers/applications/", puis sélectionner l'application.
# Ensuite, il faut aller dans l'onglet "Bot". Le token se trouve à droite de la photo de profil du bot.
DISCORD_BOT_TOKEN = "OTk5MjIyOTEyNjg0OTM3MjY2.G2WG4P.aQg3lqPmRMtxVWJ-TY_HsfTqVID34pwdEAD4Ic"
