"""
Ouvrir une page internet
"""

import time
from selenium import webdriver

def setup_chrome():
    """
    Test de connexion - Ouverture de notre gitlab sur Chrome
    """
    options = webdriver.ChromeOptions()
    options.add_experimental_option('excludeSwitches', ['enable-logging'])
    browser = webdriver.Chrome(options=options)
    print("[+] Test - Ouverture de notre gitlab")
    browser.get('https://gitlab.com/Acenox/projet_ESGI')
    time.sleep(5)
    print("[+] Fermeture du gitlab")
    browser.quit()
